
const mg = require('../../../service/email/mailgun');

var expect    = require("chai").expect;

process.env.NODE_ENV = "local";

describe('mailgun', function() {

    describe('mailgun.send', function () {
        it("returns the response", function (done) {
            mg("will.lee.sesame@gmail.com","will.lee.sesame@gmail.com", "hello", "hello")
                .then(res => {
                expect(res.message).to.equal('Queued. Thank you.');
                done();
            })

        })

    })

    describe('mailgun.send.multiple', function () {
        it("returns the response", function (done) {
            mg("will.lee.sesame@gmail.com","will.lee.sesame@gmail.com", "hello", "hello",["will.lee.sesame@gmail.com","will.lee.sesame@gmail.com"],["will.lee.sesame@gmail.com", "will.lee.sesame@gmail.com"])
                .then(res => {
                expect(res.message).to.equal('Queued. Thank you.');
            done();
        })
        })

    })

    describe('mailgun.send.400', function () {
        it("returns 400 response", function (done) {
            mg("will.lee.sesame@gmail.com","will.lee.sesame@gmail.com", null, null)
                .catch( res=>{
                expect(res.message).to.equal('400 - {"message":"Need at least one of \'text\' or \'html\' parameters specified"}');
                done();
            });
        })
    })

    describe('mailgun.send.mock.error', function () {
        it("returns mocked error error", function (done) {

            var Bluebird = require('bluebird');
            var sinon = require('sinon');

            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            // calls to rp.post() return a promise that will resolve to an object
            rpStub.returns(Bluebird.reject({
                error: 'timeout'
            }));

            mg("will.lee.sesame@gmail.com","will.lee.sesame@gmail.com", null, null)
                .catch( res=>{
                expect(res.rawResponse.error).to.equal('timeout');
            done();
        });
        })
    })

    describe('send.mock', function () {
        it("returns the response", function (done) {
            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            rpStub.returns(Bluebird.resolve({ id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                message: 'Queued. Thank you.',
                status: 'queued',
                rawResponse:
                { id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                    message: 'Queued. Thank you.' } }
            ));

            emailService.send("will.lee.sesame@gmail.com", "will.lee.sesame@gmail.com", "hello", "hello")
                .then(res => {
                console.log(res)
            expect(res.message).to.equal('Queued. Thank you.');
            require('request-promise').post.restore()
            return done();
        })
        })
    });

    describe('send.mock.failed', function () {
        it("returns the response", function (done) {
            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            rpStub.returns(Bluebird.reject({ error:'timeout' }));

            emailService.send("will.lee.sesame@gmail.com", "will.lee.sesame@gmail.com", "hello", "hello")
                .catch(res => {
                console.log(res)
            expect(res.rawResponse.error).to.equal('timeout');
            require('request-promise').post.restore()
            return done();
        })
        })
    });


    afterEach(()=>{

    })

})


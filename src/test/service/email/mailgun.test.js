
const mg = require('../../../service/email/mailgun');

var expect    = require("chai").expect;

process.env.NODE_ENV = "local";

describe('mailgun', function() {


    describe('mailgun.send.mock.error', function () {
        it("returns mocked error", function (done) {

            var Bluebird = require('bluebird');
            var sinon = require('sinon');

            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            // calls to rp.post() return a promise that will resolve to an object
            rpStub.returns(Bluebird.reject({
                message: 'timeout'
            }));

            mg.send("will.lee.sesame@gmail.com","will.lee.sesame@gmail.com", null, null)
                .catch( res=>{
                expect(res).to.equal('timeout');
            done();
        });
        })
    })

    describe('mailgun.send.mock.sent', function () {
        it("returns mocked sent", function (done) {

            var Bluebird = require('bluebird');
            var sinon = require('sinon');

            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            // calls to rp.post() return a promise that will resolve to an object
            rpStub.returns(Bluebird.resolve({ id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                message: 'Queued. Thank you.',
                status: 'queued',
                rawResponse:
                { id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                    message: 'Queued. Thank you.' } }
            ));

            mg.send("will.lee.sesame@gmail.com","will.lee.sesame@gmail.com", null, null)
                .then( res=>{
                expect(res.message).to.equal('Queued. Thank you.');
                done();
        });
        })
    })


    afterEach(()=>{
        require('request-promise').post.restore()
    })

})



const emailService = require('../../../service/email');

var expect    = require("chai").expect;

process.env.NODE_ENV = "local";


var Bluebird = require('bluebird');
var sinon = require('sinon');




describe('emailService.ut', function() {

    describe('send.email.sendgrid.failed.mailgun.ok', function () {
        it("returns the response", function (done) {
            sinon.stub(require('../../../service/email/sendgrid'),'send').returns(Bluebird.reject({
                message: 'service down'})
            );

            sinon.stub(require('../../../service/email/mailgun'),'send').returns(Bluebird.resolve({ id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                message: 'Queued. Thank you.',
                status: 'queued',
                rawResponse:
                { id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                    message: 'Queued. Thank you.' }
                })
            );
            emailService.send("will.lee.sesame@gmail.com", "will.lee.sesame@gmail.com", "hello", "hello")
                .then(res => {
                expect(res.message).to.equal('Queued. Thank you.');
                require('../../../service/email/sendgrid').send.restore();
                require('../../../service/email/mailgun').send.restore();
                return done();
            })
        })
    });

    describe('send.email.sendgrid.ok.mailgun.failed', function () {
        it("returns the response", function (done) {
            sinon.stub(require('../../../service/email/mailgun'),'send').returns(Bluebird.reject({
                message: 'service down'})
            );

            sinon.stub(require('../../../service/email/sendgrid'),'send').returns(Bluebird.resolve({ id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                    message: 'Queued. Thank you.',
                    status: 'queued',
                    rawResponse:
                    { id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                        message: 'Queued. Thank you.' }
                })
            );
            emailService.send("will.lee.sesame@gmail.com", "will.lee.sesame@gmail.com", "hello", "hello")
                .then(res => {
                expect(res.message).to.equal('Queued. Thank you.');
                require('../../../service/email/sendgrid').send.restore();
                require('../../../service/email/mailgun').send.restore();
                return done();
        })
        })
    });

    describe('send.email.sendgrid.ok.mailgun.failed', function () {
        it("returns the response", function (done) {
            sinon.stub(require('../../../service/email/mailgun'),'send').returns(Bluebird.reject({
                message: 'service down'})
            );

            sinon.stub(require('../../../service/email/sendgrid'),'send').returns(Bluebird.reject({
                message: 'service down'})
            );
            emailService.send("will.lee.sesame@gmail.com", "will.lee.sesame@gmail.com", "hello", "hello")
                .catch(res => {
                expect(res.message).to.equal('service down');
            require('../../../service/email/sendgrid').send.restore();
            require('../../../service/email/mailgun').send.restore();
            return done();
        })
        })
    });



})


/**
 * Created by will on 18/11/2017.
 */
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var request = require('request')
var expect    = require("chai").expect;
const port = 10002;
var server = require('../server')(require('../schema'),port);;



const  createApolloFetch  = require('apollo-fetch').createApolloFetch;

var url = `http://localhost:${port}/graphql`;

const fetch = createApolloFetch({
    uri: url,
});

var Bluebird = require('bluebird');
var sinon = require('sinon');

describe('graphql.it', function() {

    describe('mutation.emailAction', function() {
        it("returns the email sent", function(done) {
            const query = `
              mutation  {
                          sendEmail(Email:{
                          from:"x@1.com" 
                           to:["will.lee.sesame@gmail.com"],
                           title: "test2",
                           body: "test2"
                          })
                          {
                          from,
                            status
                          }
                        }
`;
            fetch({ query }).then(res => {
                console.log(res)
            const { data, errors, extensions } = res;
            expect(res.data.sendEmail.from).to.equal('x@1.com');
            expect(res.data.sendEmail.status).to.equal('queued');
            expect(res.data.sendEmail.errors).to.equal(undefined);
            done();
        });
        })
    })


    describe('mutation.emailAction with wrong email', function() {
        it("returns the invalid response", function( done) {
            const query = `
              mutation  {
                          sendEmail(Email:{
                          from:"1@.com",
                          to:["will.lee.sesame@gmail.com","will.lee.sesame@gmail.com"]
                          })
                          {
                          from,
                            status
                          }
                        }
`;
            fetch({ query }).then(res => {
                console.log(res)
            const { data, errors, extensions } = res;
            expect(errors[0].message).to.equal('Query error: Not a valid Email address');
            done();
        }).catch( e => {
                console.log(e);
            done();
            });
        })
    })


})


after(function() {
    server.close();
});
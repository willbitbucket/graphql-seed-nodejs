/**
 * Created by will on 18/11/2017.
 */
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var request = require('request')
var expect    = require("chai").expect;
const port = 10002;
var server = require('../server')(require('../schema'),port);;



const  createApolloFetch  = require('apollo-fetch').createApolloFetch;

var url = `http://localhost:${port}/graphql`;

const fetch = createApolloFetch({
    uri: url,
});

var Bluebird = require('bluebird');
var sinon = require('sinon');



describe('graphql.ut', function() {


    describe('mutation.emailAction.missing.to', function() {
        it("returns the email sent", function(done) {
            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            rpStub.returns(Bluebird.reject(
                { id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                    message: "400 - {\"message\":\"'to' parameter is missing\"}" }
            ));

            const query = `
              mutation  {
                          sendEmail(Email:{
                          from:"x@2.com" 
                           title: "test2",
                           body: "test2"
                          })
                          {
                          from,
                            status
                          }
                        }
`;
            fetch({ query }).then(res => {
                const { data, errors, extensions } = res;
                expect(res.data.sendEmail).to.equal(null);
                expect(res.errors[0].message).to.equal("400 - {\"message\":\"'to' parameter is missing\"}");
                require('request-promise').post.restore();
                done();
        });
        })
    })

    describe('mutation.emailAction.missing.to', function() {
        it("returns the email sent", function(done) {
            var stub = sinon.stub(require('../service/email'), 'send'); // mock rp.post() calls

            stub.returns(Bluebird.reject(
                { message: "400 - {\"message\":\"'to' parameter is missing\"}" })
            );

            const query = `
              mutation  {
                          sendEmail(Email:{
                          from:"x@2.com" 
                           title: "test2",
                           body: "test2"
                          })
                          {
                          from,
                            status
                          }
                        }
`;
            fetch({ query }).then(res => {
                const { data, errors, extensions } = res;
            expect(res.data.sendEmail).to.equal(null);
            expect(res.errors[0].message).to.equal("400 - {\"message\":\"'to' parameter is missing\"}");

            require('../service/email').send.restore()
            done();
        });
        })
    })


    describe('mutation.emailAction', function() {
        it("returns the email sent", function(done) {
            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            rpStub.returns(Bluebird.resolve({ id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                message: 'Queued. Thank you.',
                status: 'queued',
                rawResponse:
                { id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                    message: 'Queued. Thank you.' } }
            ));

            const query = `
              mutation  {
                          sendEmail(Email:{
                          from:"x@1.com" 
                           to:["will.lee.sesame@gmail.com"],
                           title: "test2",
                           body: "test2"
                          })
                          {
                          from,
                            status
                          }
                        }
`;
            fetch({ query }).then(res => {
                console.log(res)
            const { data, errors, extensions } = res;
            expect(res.data.sendEmail.from).to.equal('x@1.com');
            expect(res.data.sendEmail.status).to.equal('queued');
            expect(res.data.sendEmail.errors).to.equal(undefined);
            require('request-promise').post.restore()
            done();
        });
        })
    })

    describe('mutation.emailAction.missing.to', function() {
        it("returns the email sent", function(done) {
            var rpStub = sinon.stub(require('request-promise'), 'post'); // mock rp.post() calls

            rpStub.returns(Bluebird.resolve({ id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                status: 'failed',
                rawResponse:
                { id: '<20171118221726.97232.0BB10EA847618EFB@sandbox8c67c88bbaa9458582ae26f9b67f0e0d.mailgun.org>',
                    message: 'missing to address' } }
            ));

            const query = `
              mutation  {
                          sendEmail(Email:{
                          from:"x@2.com" 
                           title: "test2",
                           body: "test2"
                          })
                          {
                          from,
                            status
                          }
                        }
`;
            fetch({ query }).then(res => {
                console.log('test')
            console.log(res)
            const { data, errors, extensions } = res;
            expect(res.data.sendEmail.from).to.equal('x@2.com');
            expect(res.data.sendEmail.status).to.equal('queued');
            expect(res.data.sendEmail.errors[0]).to.equal(undefined);
            require('request-promise').post.restore()
            done();
        });
        })
    })

    describe('mutation.emailAction with wrong email', function() {
     it("returns the invalid response", function( done) {
     const query = `
     mutation  {
     sendEmail(Email:{
     from:"1@.com",
     to:["will.lee.sesame@gmail.com","will.lee.sesame@gmail.com"]
     })
     {
     from,
     status
     }
     }
     `;
     fetch({ query }).then(res => {
                 console.log(res)
                 const { data, errors, extensions } = res;
                 expect(errors[0].message).to.equal('Query error: Not a valid Email address');
                 done();

            });
        })
     })


})
after(function() {
    server.close();
});
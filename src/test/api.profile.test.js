/**
 * Created by will on 18/11/2017.
 */
var chai = require('chai');
var chaiHttp = require('chai-http');
var should = chai.should();
var request = require('request')
var expect    = require("chai").expect;

var server = require('../server')(require('../schema'),8080);;



const  createApolloFetch  = require('apollo-fetch').createApolloFetch;

var url = "http://localhost:8080/graphql";

const fetch = createApolloFetch({
    uri: url,
});



describe('graphql', function() {
    describe('query.customerProfile', function() {
        it("returns customProfile", function(done) {
            fetch({
                query: `query {
                      customerProfile(email:"peter.liu@x.com") {
                        id, birthDate, email
                      }
                    }`,
            }).then(res => {
                console.log(res)
                expect(res.data.customerProfile.email).to.equal("peter.liu@x.com");
                done();
        });
     })
    });

    describe('mutation.customerProfile', function() {
        it("returns the upserted user", function(done) {
            const query = `
              mutation  { upsertProfile(Profile:{
                id:"002",
                birthDate:"2000-02-01"
              })
              {
                id,
                birthDate,
                firstName
              }
            }
`;
           fetch({ query }).then(res => {
                console.log(res)
                expect(res.data.upsertProfile.birthDate).to.equal('2000-02-01');
                done();
        });
        })
    })

})

after(function() {
    server.close();
});
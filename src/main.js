const schema = require('./schema');

const graphqlServer = require('./server');
const log4js = require('log4js');
const config = require('config');
var app = graphqlServer(schema, 8090);

var logger = log4js.getLogger();
logger.level = config.get("loggerLevel")?config.get("loggerLevel"):"info";


module.exports = app; //for testing
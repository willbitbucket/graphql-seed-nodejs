const express = require('express'),
    graphqlHTTP = require('express-graphql'),
    morgan = require('morgan'),
    cors = require('cors');

var path = require('path');
// parse application/json
var bodyParser = require('body-parser')
module.exports = function(schema, port) {
  var app = express();

  app.use(morgan('combined'));
  app.use(express.static('public'));
  app.use('/graphql', cors()); // warning: enables all CORS requests
  /*app.use(bodyParser.json())
  app.use('/graphql', function(req, res, next){
    console.log(req.body);
    next()
  });*/

  app.use('/graphql', graphqlHTTP({ schema, graphiql: true }));

  app.use('/schema', function(req, res, _next) {
    var printSchema = require('graphql/utilities/schemaPrinter').printSchema;
    res.set('Content-Type', 'text/plain');
    res.send(printSchema(schema));
  });

  var server = app.listen(port, "0.0.0.0");
  console.log(`Started on http://localhost:${port}/`);
  return server;//for testing
}

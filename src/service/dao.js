
const peter = {
  id: '001',
  firstName: 'peter',
  lastName: 'liu',
  birthDate: '1981-08-01',
  home: '123 phenix st, sydney',
  office: 'mascot',
  email:'peter.liu@x.com'
}

var profiles = [peter];


/**
 * Helper function to get a character by ID.
 */
function getProfileByKey(key, value) {
  // Returning a promise just to illustrate GraphQL.js's support.
  for(var i = 0; i<profiles.length; i++)
      {
        if (profiles[i][key] === value) return Promise.resolve(profiles[i]);
      }
      return Promise.resolve(null);
}

function upsertProfile(id, object){
  if(id)
  {
    for(var i = 0; i<profiles.length; i++)
    {
      if (profiles[i].id === id) {
        profile = profiles[i];
        profiles[i] = Object.assign({}, profile, object);
        return Promise.resolve(profiles[i]);
      }
    }
    profiles.push(object);
    return Promise.resolve(object);
  }

}

module.exports = {
  getProfileByKey: getProfileByKey,
  upsertProfile: upsertProfile
}
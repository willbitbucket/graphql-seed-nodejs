/**
 * Created by will on 18/11/2017.
 */
var logger = require('log4js').getLogger();

function send(from, to,  title, body, cc, bcc){
    return require('./sendgrid').send(from, to,  title, body, cc, bcc)
        .then(res=>{return res})
            .catch(e=>{
                logger.info("sendgrid is down, swith to next provider");
                return require('./mailgun').send(from, to,  title, body, cc, bcc)
                       .then(res=>{ return res;})
                       .catch( e=>{
                            logger.info("mailgun is down, no more provider, failing...");
                            throw e;
                        })
                    })

}
module.exports = {
    send: send
};
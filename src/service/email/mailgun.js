/**
 * Created by will on 18/11/2017.
 */
const config = require('config');
const url = config.get('integrations.email.mailgun.url');
const apiKey = config.get('integrations.email.mailgun.apiKey');
if(!url || !apiKey) throw Error("missing integrations.email.mailgun.url or integrations.email.mailgun.apiKey in config")

const constant = require('../../contract/constant');

var logger = require('log4js').getLogger();

const rp = require('request-promise');

function send(from, to,  title, body, cc, bcc){
    //https://documentation.mailgun.com/en/latest/user_manual.html#sending-via-api
    // need send url-encoded-form, NOT json

    var paylaod = {
        form: {
            from: from,
            to: to,
            title: title,
            text: body,
            cc: cc,
            bcc: bcc
        },
        simple: true, //reject non-2xx as error,
        json:true
    };
    return rp.post(url, paylaod).then(res =>{
            logger.info('send by mailgun');
            return {
                id: res.id,
                message: res.message,
                status: constant.emailStatus.queued,
                rawResponse: res,
            }
        }).catch(res =>{
            logger.warn("mailgun exception", res);
            throw res.message;

    });
}

module.exports.send = send ;
/**
 * Created by will on 18/11/2017.
 */
const config = require('config');
const url = config.get('integrations.email.sendgrid.url');
const apiKey = config.get('integrations.email.sendgrid.apiKey');
if(!url || !apiKey) throw Error("missing integrations.email.sendgrid.url or integrations.email.sendgrid.apiKey in config")

const constant = require('../../contract/constant');

/*
paylaod sample
 Authorization:Bearer <tokan>

 {
 "personalizations": [
 {
 "to": [
 {
 "email": "will.lee.sesame@gmail.com"
 }
 ],
 "subject": "Hello, World!"
 }
 ],
 "from": {
 "email": "will.lee.sesame@gmail.com"
 },
 "content": [
 {
 "type": "text/plain",
 "value": "Hello, World!"
 }
 ]
 }
 */
const rp = require('request-promise');
var logger = require('log4js').getLogger();
function send(from, to,  title, body, cc, bcc){

    var options = {
        method: 'POST',
        uri: url,
        headers: {
            'Authorization': 'Bearer '+ apiKey,
            'Content-Type': 'application/json'
        },
        body: {
            personalizations: [
                {
                    "to": to.map(e =>{return {email:e};}),
                    "cc": cc==null||cc.length==0?null:cc.map(e =>{return {email:e};}),
                    "bcc": bcc==null||bcc.length==0?null:bcc.map(e =>{return {email:e};}),
                    "subject": title
                }
            ],
            from: {
                "email": from
            },
            content: [
                {
                    "type": "text/plain",
                    "value": body
                }
            ]
        },
        simple: true, //reject non-2xx as error,
        json: true // Automatically stringifies the body to JSON
    };
    //logger.debug(JSON.stringify(options));
    return rp(options).then(res =>{
            logger.info('send by sendgrid');
            return {
                id: null,
                message: null,
                status: constant.emailStatus.queued,
                rawResponse: res,
            }
        }).catch(res =>{
            logger.warn("sendgrid exception", res);
            throw res.error.errors.reduce((acc,e) => {
                return acc+e.message
            }, '');
        });
}

module.exports.send = send;
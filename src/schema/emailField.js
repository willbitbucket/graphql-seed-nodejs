const graphql = require('graphql');
const
    GraphQLEnumType = graphql.GraphQLEnumType,
    GraphQLInterfaceType = graphql.GraphQLInterfaceType,
    GraphQLObjectType = graphql.GraphQLObjectType,
    GraphQLList = graphql.GraphQLList,
    GraphQLNonNull = graphql.GraphQLNonNull,
    GraphQLSchema = graphql.GraphQLSchema,
    GraphQLString = graphql.GraphQLString,
    GraphQLInputObjectType = graphql.GraphQLInputObjectType;


const GraphQLEmail =require('graphql-custom-types').GraphQLEmail;

const constant = require('../contract/constant');

//Email  includes
// from
// to
// cc
// bcc
// title
// body

var emailStatus = new GraphQLEnumType({
    name: 'EmailStatus',
    description: 'Status of the email',
    values: {
        queued: {
            value: constant.emailStatus.queued,
            description: 'Queued in service, may or may not be received by recipients.',
        },
        failed: {
            value: constant.emailStatus.failed,
            description: 'Failed to be delivered.',
        }
    }
});

const EmailType = new GraphQLObjectType({
        name: 'EmailAction',
        description: 'A customer profile contains following varialbes',
        fields: () => ({
        from: {
            type: new GraphQLNonNull(GraphQLEmail),
            description: 'The sender email address.',
        },
        to: {
            type: new GraphQLList(GraphQLEmail),
            description: 'Recipienpts\' email list.',
        },
        cc: {
            type: new GraphQLList(GraphQLEmail),
            description: 'CC email list.',
        },
        bcc: {
            type: new GraphQLList(GraphQLEmail),
            description: 'BCC email list.',
        },
        title: {
            type: GraphQLString,
            description: 'Email title.',
        },
        body: {
            type: GraphQLString,
            description: 'Email content.',
        },
        status:{
            type: emailStatus,
            description: 'Email content.',
        }
    })
})
;

const EmailInputType = new GraphQLInputObjectType({
        name: 'EmailInput',
        description: 'A customer profile contains following varialbes',
        fields: () => ({
        from: {
            type: new GraphQLNonNull(GraphQLEmail),
            description: 'The sender email address.',
        },
        to: {
            type: new GraphQLList(GraphQLEmail),
            description: 'Recipienpts\' email list.',
        },
        cc: {
            type: new GraphQLList(GraphQLEmail),
            description: 'CC email list.',
        },
        bcc: {
            type: new GraphQLList(GraphQLEmail),
            description: 'BCC email list.',
        },
        title: {
            type: GraphQLString,
            description: 'Email title.',
        },
        body: {
            type: GraphQLString,
            description: 'Email content.',
        },
    })
})
;


var emailService = require('../service/email');

const sendEmail = {
                type: EmailType,
                description: 'Send an email.',
                args: {
                    Email: {
                        type: new GraphQLNonNull(EmailInputType),
                    },
                },
                resolve: (rootValue, input) => {
                    console.log(input.Email);
                    var {from, to,  title, body, cc, bcc} = input.Email;
                    return emailService.send(from, to,  title, body, cc, bcc)
                        .then(res =>{
                        console.log(res);
                        input.Email.status = res.status;
                        return Promise.resolve(input.Email);
                    });

                },
        };





module.exports = {
    sendEmail: sendEmail
}


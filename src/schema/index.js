/**
 * Created by will on 18/11/2017.
 */
const
    graphql = require('graphql'),
    GraphQLSchema = graphql.GraphQLSchema,
    GraphQLObjectType = graphql.GraphQLObjectType;

const
    profileField = require('./profileField.js');

const emailField = require('./emailField');

var schema =  new GraphQLSchema({
        query: new GraphQLObjectType({
                name: 'Query',
                description: "The root for all query objects",
                fields: () => ({
                customerProfile: profileField.customerProfileQueryField,
            })
        }),
        mutation: new GraphQLObjectType({
            name: 'Mutation',
            description: "The root for all mutations",
            fields: () => ({
            upsertProfile: profileField.upsertProfile,
            sendEmail: emailField.sendEmail
        })
    })
});

module.exports = schema
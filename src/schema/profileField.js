const graphql = require('graphql');
const
    GraphQLEnumType = graphql.GraphQLEnumType,
    GraphQLInterfaceType = graphql.GraphQLInterfaceType,
    GraphQLObjectType = graphql.GraphQLObjectType,
    GraphQLList = graphql.GraphQLList,
    GraphQLNonNull = graphql.GraphQLNonNull,
    GraphQLSchema = graphql.GraphQLSchema,
    GraphQLString = graphql.GraphQLString,
    GraphQLInputObjectType = graphql.GraphQLInputObjectType;

const dao = require('../service/dao');

const GraphQLDate =require('graphql-iso-date').GraphQLDate;


//Customer profile includes
// first name
// last name
// date of birth
// address (different
//  type like home, office, email etc.)

const ProfileType = new GraphQLObjectType({
        name: 'Profile',
        description: 'A customer profile contains following varialbes',
        fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The id of the profile.',
        },
        firstName: {
            type: GraphQLString,
            description: 'The firstName of the profile.',
        },
        lastName: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
        birthDate: {
            type: GraphQLDate,
            description: 'The birthday of the profile.',
        },
        email: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
        home: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
        office: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
    })
})
;


const ProfileInputType = new GraphQLInputObjectType({
        name: 'CustomerProfileInput',
        description: 'Upsert parameters for the customer profile',
        fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'The id of the profile.',
        },
        firstName: {
            type: GraphQLString,
            description: 'The firstName of the profile.',
        },
        lastName: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
        birthDate: {
            type: GraphQLDate,
            description: 'The birthday of the profile.',
        },
        email: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
        home: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
        office: {
            type: GraphQLString,
            description: 'The lastName of the profile.',
        },
    }),
});



const upsertProfile = {
                type: ProfileType,
                description: 'Create or update a profile by id, returns the profile after the upsert',
                args: {
                    Profile: {
                        type: new GraphQLNonNull(ProfileInputType),
                    },
                },
                resolve: (rootValue, input) => {
                if (!input.Profile.id)
                {
                    throw new Error('Id is required');
                }
                return dao.upsertProfile(input.Profile.id, input.Profile);
                },
        };


var customerProfileQueryField = {
            type: ProfileType,
            description: 'Query a profile by id, firstName, lastName, birthDate, email, home, office etc',
            args: {
                id: {
                    description: 'id of the profile',
                    type: GraphQLString,
                },
                firstName: {
                    type: GraphQLString,
                    description: 'The firstName of the profile.',
                },
                lastName: {
                    type: GraphQLString,
                    description: 'The lastName of the profile.',
                },
                birthDate: {
                    type: GraphQLDate,
                },
                email: {
                    type: GraphQLString,
                    description: 'The lastName of the profile.',
                },
                home: {
                    type: GraphQLString,
                    description: 'The lastName of the profile.',
                },
                office: {
                    type: GraphQLString,
                    description: 'The lastName of the profile.',
                }
            },
            resolve: (root, input) => {
                if (input.id) return dao.getProfileByKey("id", input.id);
                else if(input.email) return dao.getProfileByKey("email", input.email);
                else if(input.birthDate) return dao.getProfileByKey("birthDate", input.birthDate);
                else if(input.lastName) return dao.getProfileByKey("lastName", input.lastName);
                else if(input.firstName) return dao.getProfileByKey("firstName", input.firstName);
                else if(input.home) return dao.getProfileByKey("home", input.home);
                else if(input.office) return dao.getProfileByKey("office", input.office);
                else return null;
            },
};




module.exports = {
    customerProfileQueryField: customerProfileQueryField,
    upsertProfile: upsertProfile
}


# graphql-seed

This is a nodejs seed GraphQL server supporing both query and mutations. It is built with express, node.js, and ES6.

# dependencies
npm install --save graphql-iso-date


# Run (`local` is the config under /config)
```
    npm install
    NODE_ENV=local npm start

```

You will now have a GraphQL endpoint at [http://localhost:8080/](http://localhost:8080/). If you visit the URL using a browser, you will see a live [GraphiQL](https://github.com/graphql/graphiql) IDE which you can use for querying. The same URL also serves as a JSON endpoint for graphql queries.

# API specification/documentation:
http://localhost:8080/graphql, click the upper right button of 'Docs'

# Example query and mutation:

```sh
query {
      customerProfile(id:"001") {
        id, birthDate
      }
    }
```

```sh
query {
      customerProfile(email:"peter.liu@x.com") {
        id, birthDate, email
      }
    }
```

```sh
mutation  {
  upsertProfile(Profile:{
    id:"002",
    birthDate:"2000-02-01"
  })
  {
    id,
    birthDate,
    firstName
  }
}
```


```
mutation  {
  sendEmail(Email:{
  from:"abc@gmail.com",
    to:["123@gmail.com"],
    body:"",
    title:""

  })
  {
  	from,
    status,
    to
  }
}
```


# High-level Architecture including security, system integration
See profile API: `./design/High-level-architecture.png`
See email API: https://drive.google.com/file/d/11IRAclGvehpOOR-2IpfRHcUr7d9y3E1P/view (Solution 0 currently)